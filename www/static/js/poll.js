!function () {
  let offset;
  let isFirstPoll = true;
  poll();

  async function poll() {
    const { count, data } = await fetch(`/a/poll?o=${offset || ''}`).then(res => res.json());
    if (count) {
      const messages = data.map(({ ip, resource: { name } }) => createElement('p', {
        textContent: `${ip} just visited ${name}.`,
      }));
      if (data.length < count) {
        messages.push(createElement('p', {
          textContent: `And ${count - data.length} more...`,
        }));
      }
      if (!isFirstPoll) showToast(...messages);
      offset = data[0].id;
    }
    await delay(5000);
    isFirstPoll = false;
    poll();
  }

  function createElement(tagName, props) {
    const el = document.createElement(tagName);
    if (props) {
      Object.entries(props)
      .forEach(([key, value]) => {
        el[key] = value;
      });
    }
    return el;
  }

  function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
  }

  async function showToast(...els) {
    const el = createElement('div', {
      className: 'toast toast-primary',
    });
    el.append(...els);
    const wrap = createElement('div', {
      className: `toast-wrap toast-wrap-top`,
    });
    wrap.append(el);
    document.body.append(wrap);
    await delay();
    wrap.classList.remove('toast-wrap-top');
    await delay(4000);
    wrap.classList.add('toast-wrap-right');
    await delay(1000);
    wrap.remove();
  }
}();
