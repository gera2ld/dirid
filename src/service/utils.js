exports.initPaginator = function (currentPage, totalPages) {
  const STEP_END = 2;
  const STEP_ACTIVE = 2;
  const separator = { title: '...' };
  const getUrl = page => `?page=${page}`;
  const pages = [
    {
      title: 'Prev',
      url: currentPage > 1 ? getUrl(currentPage - 1) : '#',
      disabled: currentPage <= 1,
    },
  ];
  const stage1 = Math.min(1 + STEP_END, currentPage - STEP_ACTIVE);
  for (let i = 1; i < stage1; i++) {
    pages.push({
      title: i,
      url: getUrl(i),
    });
  }
  if (stage1 < currentPage - 2) {
    pages.push(separator);
  }
  for (let i = Math.max(1, currentPage - STEP_ACTIVE); i < currentPage; i++) {
    pages.push({
      title: i,
      url: getUrl(i),
    });
  }
  pages.push({
    title: currentPage,
    url: '#',
    active: true,
  });
  const stage2 = Math.min(currentPage + STEP_ACTIVE, totalPages) + 1;
  for (let i = currentPage + 1; i < stage2; i++) {
    pages.push({
      title: i,
      url: getUrl(i),
    });
  }
  if (stage2 < totalPages) {
    pages.push(separator);
  }
  const stage3 = Math.max(stage2, totalPages - STEP_END);
  for (let i = stage3; i < totalPages + 1; i++) {
    pages.push({
      title: i,
      url: getUrl(i),
    });
  }
  pages.push({
    title: 'Next',
    url: currentPage < totalPages ? getUrl(currentPage + 1) : '#',
    disabled: currentPage >= totalPages,
  });
  return pages;
};
