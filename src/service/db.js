const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: './data/.sqlite',
  },
  useNullAsDefault: true,
  debug: think.env === 'development',
});
const bookshelf = require('bookshelf')(knex);
bookshelf.plugin('pagination');

const Resource = bookshelf.Model.extend({
  tableName: 'resource',
  visits() {
    return this.hasMany(Visit);
  },
});

const Visit = bookshelf.Model.extend({
  tableName: 'visit',
  resource() {
    return this.belongsTo(Resource);
  },
});

module.exports = {
  bookshelf,
  Resource,
  Visit,
};
