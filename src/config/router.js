module.exports = [
  ['/a/poll', '/api/poll'],
  ['/:slug', '/detail/visit'],
  ['/s/:slug', '/detail/index'],
  ['/~/new', '/detail/edit'],
  ['/~/:id', '/detail/index'],
  ['/~/:id/edit', '/detail/edit'],
];
