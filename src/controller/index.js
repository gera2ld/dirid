const Base = require('./base');

module.exports = class extends Base {
  async indexAction() {
    const { page, per } = this.ctx.query;
    const db = this.service('db');
    const { Resource } = db;
    const result = await Resource
    .fetchPage({
      pageSize: per || 25,
      page,
    });
    this.assign('items', result.models.map(model => model.toJSON()));
    const { pagination: { pageCount, page: currentPage } } = result;
    if (pageCount > 1) {
      const utils = this.service('utils');
      this.assign('pages', utils.initPaginator(currentPage, pageCount));
    }
    return this.display();
  }
};
