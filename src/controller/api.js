const Base = require('./base');

module.exports = class extends Base {
  async pollAction() {
    const offset = this.get('o');
    const { Visit } = this.service('db');
    let Query = Visit.forge();
    if (offset) Query = Query.where('id', '>', offset);
    const [count, visits] = await Promise.all([
      Query.clone().count(),
      Query.orderBy('id', 'DESC').fetchAll({
        limit: 3,
        withRelated: ['resource'],
      }),
    ]);
    return this.json({ count, data: visits });
  }
};
