const { URL } = require('url');
const Base = require('./base.js');

const required = {
  test(str) {
    return !!str;
  },
  message() {
    return `"${this.title}" is required!`;
  },
};
const fields = [
  {
    title: 'Name',
    key: 'name',
    modifiable: true,
    rules: [required],
  },
  {
    title: 'Slug',
    key: 'slug',
    modifiable: true,
    rules: [
      required,
      {
        test(str) {
          return /^\w+$/.test(str);
        },
        message() {
          return `Invalid "${this.title}"!`;
        },
      },
    ],
  },
  {
    title: 'Detail',
    key: 'data',
    modifiable: true,
    rules: [required],
  },
  {
    title: 'Visit times',
    key: 'visit_times',
  },
];

module.exports = class extends Base {
  async getDetail(id) {
    const { Resource } = this.service('db');
    const model = await Resource.where({ id }).fetch();
    if (!model) this.ctx.throw(404);
    return model.toJSON();
  }

  async getModelBySlug(slug) {
    const { Resource } = this.service('db');
    const model = await Resource.where({ slug }).fetch();
    if (!model) this.ctx.throw(404);
    return model;
  }

  async putDetail(id, data) {
    if (data) {
      const { Resource } = this.service('db');
      if (id) {
        // update
        await Resource.forge({ id }).save(data);
      } else {
        // create
        const model = await Resource.forge().save(data);
        id = model.get('id');
      }
    }
    return id;
  }

  /**
   * index action
   * @return {Promise} []
   */
  async indexAction() {
    const id = this.get('id');
    if (id) {
      const model = await this.getDetail(id);
      this.assign({ model });
      return this.display();
    } else {
      const slug = this.get('slug');
      if (slug) {
        const model = await this.getModelBySlug(slug);
        return this.redirect(`/~/${model.get('id')}`);
      }
    }
  }

  async editAction() {
    const errors = {
      fields: {},
      data: [],
    };
    let data;
    const id = this.get('id');
    if (this.isPost) {
      data = {};
      fields.forEach(field => {
        if (!field.modifiable) return;
        const { key } = field;
        const value = this.post(key);
        data[key] = value;
        if (field.rules) {
          field.rules.some(({ test, message }) => {
            const ok = test.call(field, value);
            if (!ok) {
              errors.fields[key] = 1;
              errors.data.push(message.call(field));
              return true;
            }
          });
        }
      });
      if (!errors.data.length) {
        try {
          const modelId = await this.putDetail(id, data);
          return this.redirect(`/~/${modelId}`);
        } catch (e) {
          if (e.message) errors.data.push(e.message);
        }
      }
    }
    const model = id ? await this.getDetail(id) : { id: 'NEW' };
    Object.assign(model, data);
    this.assign({ model, errors, fields });
    return this.display();
  }

  async visitAction() {
    const slug = this.get('slug');
    const model = await this.getModelBySlug(slug);
    const { ip } = this.ctx;
    const { bookshelf, Resource, Visit } = this.service('db');
    // do not await
    Resource.where({ slug }).save({
      visit_times: bookshelf.knex.raw('visit_times + 1'),
    }, {
      method: 'update',
    });
    Visit.forge().save({
      ip,
      resource_id: model.get('id'),
    });
    const url = new URL(model.get('data'));
    if (['http:', 'https:'].includes(url.protocol)) return this.redirect(url.href);
    if (url.protocol === 'file:') return this.download(url.pathname);
  }
};
