module.exports = {
  extends: 'think',
  rules: {
    'comma-dangle': ['error', 'always-multiline'],
  },
};
